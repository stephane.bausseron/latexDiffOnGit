#!/usr/bin/env python2.7

import argparse
import os, shutil
from git import Git
from subprocess import call

join = os.path.join

parser = argparse.ArgumentParser(description='LDOG is a LatexDiff wrapper for tex management over Git.')
parser.add_argument('-o', "--original", dest='original', required=True, help="Hash of the original commit")
parser.add_argument('-m', "--modified", dest='modified', required=True, help="Hash of the modified commit")
parser.add_argument('-r', "--repo", dest='repo', required=True, help="Path of the repo")
parser.add_argument('-f', "--file", dest='file', required=True, help="Path of the file")
parser.add_argument('-d', "--dfile", dest='dfile', required=True, help="Output Diff file")

args = parser.parse_args()

repo = Git(args.repo)

tmpOrig = 'tmpOriginal.tex'
tmpMod = 'tmpModified.tex'

repo.checkout(args.original)
shutil.copy2(join(args.repo, args.file), tmpOrig)

repo.checkout(args.modified)
shutil.copy2(join(args.repo, args.file), tmpMod)

call("latexdiff %s %s > %s" % (tmpOrig, tmpMod, args.dfile), shell=True)

os.remove(tmpOrig)
os.remove(tmpMod)
