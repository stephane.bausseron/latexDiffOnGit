LDOG (L-DOG) is a Latex Diff Over Git

Here's a sample:
./genDiff.py -o 5d193ffd861fa7f713939a925df05f849b043686
-m 3cad71372e8f1cc008d9d2dd961c2f9e9d81701a -f samples/file.tex -r . -d output.tex

For Help: ./genDiff.py -h